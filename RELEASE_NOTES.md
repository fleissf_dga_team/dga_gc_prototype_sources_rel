# FLEISS Software Foundation

# DGA MMORPG Game Client Prototype

## Release Notes

### Version 0.1
19 September 2018

The version 0.1 is intended to test capability of the jMonkeyEngine game engine to carry the DGA game client with acceptable productivity. It includes the feature-complete core object model of the DGA game client providing basic game functionality.
