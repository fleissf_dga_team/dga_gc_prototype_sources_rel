/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.scene.Spatial;

/**
 * The abstract implementation for game characters having spatial data.
 *
 * @extends java.lang.Object
 * @implements DgaJmeSpatial
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 11.5.2018
 */
public abstract class AbstractDgaJmeSpatialable extends Object implements DgaJmeSpatialable {
    private Spatial spatial_ = null;
    private DgaJmeScale scale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param spatial The spatial.
     * @param scale The spatial's scale.
     */
    public AbstractDgaJmeSpatialable(final Spatial spatial, final DgaJmeScale scale) {
        super();
        spatial_ = spatial;
        scale_ = scale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the spatial.
     * 
     * @return Spatial The spatial.
     */
    @Override
    public Spatial getSpatial() {
        return spatial_;
    }
    
    /**
     * The method returns the spatial's scale.
     * 
     * @return DgaJmeScale The spatial's scale.
     */
    @Override
    public DgaJmeScale getScale() {
        return scale_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
