/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant;

import org.dga.client.DefaultDgaJmeScale;
import com.jme3.app.state.AbstractAppState;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a game character participating in combat.
 * 
 * @extends AbstractAppState
 * @implements StateKombattant
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.07.2018
 */
public abstract class AbstractKombattant extends AbstractAppState implements StateKombattant {
    private DgaJmeScale kombattantScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    private boolean isOnCombat_ = false;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param kombattantScale The combat participator's scale.
     */
    public AbstractKombattant(final DgaJmeScale kombattantScale) {
        super();
        kombattantScale_ = kombattantScale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the game character's scale.
     * 
     * @return DgaJmeScale The game character's scale.
     */
    @Override
    public DgaJmeScale getScale() {
        return kombattantScale_;
    }
    
    /**
     * The method returns the flag whether the combat participator is on combat or not.
     * 
     * @return boolean The flag whether the combat participator is on combat or not.
     */
    @Override
    public boolean isOnCombat() {
        return isOnCombat_;
    }
    
    /**
     * The method sets the flag whether the combat participator is on combat or not.
     * 
     * @param isOnCombat The flag whether the combat participator is on combat or not.
     */
    @Override
    public void setIsOnCombat(final boolean isOnCombat) {
        if (isOnCombat_ != isOnCombat) {
            isOnCombat_ = isOnCombat;
        }
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}