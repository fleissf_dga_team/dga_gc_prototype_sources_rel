/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import org.dga.client.DefaultDgaJmeControlBinding;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import org.dga.client.DgaJmeControlBinding;

/**
 * The control bindings for caterpillar fighting vehicles.
 *
 * @param <T> The type of a game control trigger.
 * @extends DefaultJmeControlBinding
 * @implements JmeControlBinding
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public class PanzerwagenControlBinding<T extends Trigger> 
    extends DefaultDgaJmeControlBinding implements DgaJmeControlBinding {
    //
    // Keyboard controls
    //
    /**
     * The keyboard control for moving the armoured fighting carriage forward.
     */
    public static final PanzerwagenControlBinding PANZER_MOVE_FORWARD_KEY = 
        new PanzerwagenControlBinding("Panzer_MoveForward_Key", new KeyTrigger(KeyInput.KEY_W));
    /**
     * The keyboard control for moving the armoured fighting carriage backward.
     */
    public static final PanzerwagenControlBinding PANZER_MOVE_BACKWARD_KEY = 
        new PanzerwagenControlBinding("Panzer_MoveBackward_Key", new KeyTrigger(KeyInput.KEY_S));
    /**
     * The keyboard control for breaking the armoured fighting carriage's movement.
     */
    public static final PanzerwagenControlBinding PANZER_MOVE_BREAK_KEY = 
        new PanzerwagenControlBinding("Panzer_MoveBreak_Key", new KeyTrigger(KeyInput.KEY_SPACE));
    /**
     * The keyboard control for rotating the armoured fighting carriage leftward.
     */
    public static final PanzerwagenControlBinding PANZER_ROTATE_LEFT_KEY = 
        new PanzerwagenControlBinding("Panzer_RotateLeft_Key", new KeyTrigger(KeyInput.KEY_A));
    /**
     * The keyboard control for rotating the armoured fighting carriage rightward.
     */
    public static final PanzerwagenControlBinding PANZER_ROTATE_RIGHT_KEY = 
        new PanzerwagenControlBinding("Panzer_RotateRight_Key", new KeyTrigger(KeyInput.KEY_D));
    /**
     * The keyboard control for rotating the armoured fighting carriage's turret to the left.
     */
    public static final PanzerwagenControlBinding PANZERTURM_TURN_LEFT_KEY = 
        new PanzerwagenControlBinding("Panzerturm_TurnLeft_Key", new KeyTrigger(KeyInput.KEY_LEFT));
    /**
     * The keyboard control for rotating the armoured fighting carriage's turret to the right.
     */
    public static final PanzerwagenControlBinding PANZERTURM_TURN_RIGHT_KEY = 
        new PanzerwagenControlBinding("Panzerturm_TurnRight_Key", new KeyTrigger(KeyInput.KEY_RIGHT));
    /**
     * The keyboard control for rotating the armoured fighting carriage's turret cannon upward.
     */
    public static final PanzerwagenControlBinding TURMKANONE_TURN_UP_KEY = 
        new PanzerwagenControlBinding("Turmkanone_TurnUp_Key", new KeyTrigger(KeyInput.KEY_UP));
    /**
     * The keyboard control for rotating the armoured fighting carriage's turret cannon downward.
     */
    public static final PanzerwagenControlBinding TURMKANONE_TURN_DOWN_KEY = 
        new PanzerwagenControlBinding("Turmkanone_TurnDown_Key", new KeyTrigger(KeyInput.KEY_DOWN));
    /**
     * The keyboard control for shooting from the armoured fighting carriage's turret cannon.
     */
    public static final PanzerwagenControlBinding TURMKANONE_SHOOT_KEY = 
        new PanzerwagenControlBinding("Turmkanone_Shoot_Key", new KeyTrigger(KeyInput.KEY_NUMPAD0));
    /**
     * The keyboard control for shooting from the armoured fighting carriage's turret machine gun.
     */
    public static final PanzerwagenControlBinding PANZERTURM_MG_SHOOT_KEY = 
        new PanzerwagenControlBinding("PanzerturmMG_Shoot_Key", new KeyTrigger(KeyInput.KEY_V));
    /**
     * The keyboard control for shooting from the armoured fighting carriage's hull machine gun.
     */
    public static final PanzerwagenControlBinding PANZERWANNE_MG_SHOOT_KEY = 
        new PanzerwagenControlBinding("PanzerwanneMG_Shoot_Key", new KeyTrigger(KeyInput.KEY_B));
    /**
     * The keyboard control for shooting from the armoured fighting carriage's smoke grenade discharger.
     */
    public static final PanzerwagenControlBinding NEBELKERZE_SHOOT_KEY = 
        new PanzerwagenControlBinding("Nebelkerze_Shoot_Key", new KeyTrigger(KeyInput.KEY_N));
    //
    // Mouse controls
    //
    /**
     * The mouse control for rotating the armoured fighting carriage's turret to the left.
     */
    public static final PanzerwagenControlBinding PANZERTURM_TURN_LEFT_MOUSE = 
        new PanzerwagenControlBinding("Panzerturm_TurnLeft_Mouse", new MouseAxisTrigger(MouseInput.AXIS_X, true));
    /**
     * The mouse control for rotating the armoured fighting carriage's turret to the right.
     */
    public static final PanzerwagenControlBinding PANZERTURM_TURN_RIGHT_MOUSE = 
        new PanzerwagenControlBinding("Panzerturm_TurnRight_Mouse", new MouseAxisTrigger(MouseInput.AXIS_X, false));
    /**
     * The mouse control for rotating the armoured fighting carriage's turret cannon upward.
     */
    public static final PanzerwagenControlBinding TURMKANONE_TURN_UP_MOUSE = 
        new PanzerwagenControlBinding("Turmkanone_TurnUp_Mouse", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
    /**
     * The mouse control for rotating the armoured fighting carriage's turret cannon downward.
     */
    public static final PanzerwagenControlBinding TURMKANONE_TURN_DOWN_MOUSE = 
        new PanzerwagenControlBinding("Turmkanone_TurnDown_Mouse", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
    /**
     * The mouse control for shooting from the armoured fighting carriage's turret cannon.
     */
    public static final PanzerwagenControlBinding TURMKANONE_SHOOT_MOUSE = 
        new PanzerwagenControlBinding("Turmkanone_Shoot_Mouse", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
    //
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     * 
     * @param name The name of a binding.
     * @param trigger The trigger of a binding.
     */
    protected PanzerwagenControlBinding(final String name, final T trigger) {
        super(name, trigger);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
