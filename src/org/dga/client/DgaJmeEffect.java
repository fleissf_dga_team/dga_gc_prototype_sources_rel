/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import java.util.List;

/**
 * The common interface for game objects receiving different types of effects.
 *
 * @extends DgaJmeNodeable
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.08.2018
 */
public interface DgaJmeEffect extends DgaJmeNodeable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the action this effect is intended for.
     * 
     * @return DgaJmeAction The action this effect is intended for.
     */
    public DgaJmeAction getAction();
    
    /**
     * The method adds a visual start effect for the effect.
     * 
     * @param visualEffect A visual start effect to the effect.
     */
    public void addVisualEffectEmit(final ParticleEmitter visualEffect);
    
    /**
     * The method removes a visual start effect from the effect.
     * 
     * @param visualEffect A visual start effect to be removed from the effect.
     */
    public void removeVisualEffectEmit(final ParticleEmitter visualEffect);
    
    /**
     * The method adds a visual end effect for the effect.
     * 
     * @param visualEffect A visual end effect to the effect.
     */
    public void addVisualEffectKill(final ParticleEmitter visualEffect);
    
    /**
     * The method removes a visual end effect from the effect.
     * 
     * @param visualEffect A visual end effect to be removed from the effect.
     */
    public void removeVisualEffectKill(final ParticleEmitter visualEffect);
    
    /**
     * The method adds an audio start effect to the effect.
     * 
     * @param audioEffect An audio start effect to the effect.
     */
    public void addAudioEffectStart(final AudioNode audioEffect);
    
    /**
     * The method removes an audio start effect from the effect.
     * 
     * @param audioEffect An audio start effect to be removed from the effect.
     */
    public void removeAudioEffectStart(final AudioNode audioEffect);
    
    /**
     * The method adds an audio end effect to the effect.
     * 
     * @param audioEffect An audio end effect to the effect.
     */
    public void addAudioEffectStop(final AudioNode audioEffect);
    
    /**
     * The method removes an audio end effect from the effect.
     * 
     * @param audioEffect An audio end effect to be removed from the effect.
     */
    public void removeAudioEffectStop(final AudioNode audioEffect);
    
    /**
     * The method returns the list of visual effects provided by the effect 
     * and executed at the beginning of the action. The list is sorted in 
     * the order in which the visual effects should be executed.
     * 
     * @return List<ParticleEmitter> The list of visual effects provided by the 
     * effect and executed at the beginning of the action. The list is 
     * sorted in the order in which the visual effects should be executed.
     */
    public List<ParticleEmitter> getVisualEffectEmitList();
    
    /**
     * The method returns the list of visual effects provided by the effect 
     * and executed at the end of the action. The list is sorted in the 
     * order in which the visual effects should be executed. Please notice that 
     * the lists of the start effects and end effects can differ.
     * 
     * @return List<ParticleEmitter> The list of visual effects provided by the 
     * effect and executed at the end of the action. The list is sorted 
     * in the order in which the visual effects should be executed. Please notice 
     * that the lists of the start effects and end effects can differ.
     */
    public List<ParticleEmitter> getVisualEffectKillList();
    
    /**
     * The method returns the list of audio effects provided by the effect 
     * and executed at the beginning of the action. The list is sorted in 
     * the order in which the audio effects should be executed.
     * 
     * @return List<ParticleEmitter> The list of audio effects provided by the 
     * effect and executed at the beginning of the action. The list is 
     * sorted in the order in which the audio effects should be executed.
     * 
     * @return List<AudioNode> .
     */
    public List<AudioNode> getAudioEffectStartList();
    
    /**
     * The method returns the list of audio effects provided by the effect 
     * and executed at the end of the action. The list is sorted in the 
     * order in which the audio effects should be executed. Please notice that 
     * the lists of the start effects and end effects can differ.
     * 
     * @return List<ParticleEmitter> The list of audio effects provided by the 
     * effect and executed at the end of the action. The list is sorted 
     * in the order in which the audio effects should be executed. Please notice 
     * that the lists of the start effects and end effects can differ.
     */
    public List<AudioNode> getAudioEffectStopList();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}