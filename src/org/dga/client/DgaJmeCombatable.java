/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import org.dga.client.kraftwerk.Kraft;
import org.dga.client.kraftwerk.Schaden;

/**
 * The common interface of active fighting capacity for game characters and objects.
 *
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 19.07.2018
 */
public interface DgaJmeCombatable extends DgaJmeState {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the combat participator's fighting capacity.
     * 
     * @return Panzerungkraft The combat participator's fighting capacity.
     */
    public Kraft getPower();
    
    /**
     * The method sets damage applying to the combat participator and diminishing 
     * its fighting capacity.
     * 
     * @param damage The damage applying to the combat participator and 
     * diminishing its fighting capacity.
     */
    public void setDamage(final Schaden damage);
    
    /**
     * The method returns the flag whether the combat participator is on combat or not.
     * 
     * @return boolean The flag whether the combat participator is on combat or not.
     */
    public boolean isOnCombat();
    
    /**
     * The method sets the flag whether the combat participator is on combat or not.
     * 
     * @param isOnCombat The flag whether the combat participator is on combat or not.
     */
    public void setIsOnCombat(final boolean isOnCombat);
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}