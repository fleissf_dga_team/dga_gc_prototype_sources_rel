/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import static org.dga.client.DgaJmeCommonConstant.IS_IN_COLLISION_DEV_MODE;

import org.dga.client.kampfplatz.Battlefield;
import org.dga.client.kampfplatz.BattlefieldModel;
import org.dga.client.kampfplatz.DefaultBattlefieldModel;
import org.dga.client.kampfplatz.koeln.Koeln1945;
import org.dga.client.kriegsteilnehmer.kampfpanzer.deu.PzKpfwVPanther.AusfD_FP.PzKpfwVPantherAusfD_FP;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.DefaultKampfpanzerModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.KampfpanzerModel;
import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.font.BitmapText;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * The jMonkeyEngine application class for the DGA Game Client.
 * This is the Main Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls.
 *
 * @extends com.jme3.app.SimpleApplication
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 27.04.2018
 */
public final class DgaJmeGameApplication extends SimpleApplication {
    // The game application's identifier.
    private final String APP_ID_ = "DGA";
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     */
    public DgaJmeGameApplication() {
        super();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The main method of the application.
     *
     * @param args The application's arguments.
     */
    public static void main(String[] args) {
        AppSettings appSettings_ = new AppSettings(true);
        appSettings_.setTitle("DGA");
        appSettings_.setSettingsDialogImage("Interface/Splash/ClientSplashScreen.png");
        try {
            Class<DgaJmeGameApplication> appClass_ = DgaJmeGameApplication.class;
            appSettings_.setIcons(
                new BufferedImage[] {
                    ImageIO.read(appClass_.getResourceAsStream("/Interface/Icons/DGAGC16.png")),
                    ImageIO.read(appClass_.getResourceAsStream("/Interface/Icons/DGAGC32.png")),
                    ImageIO.read(appClass_.getResourceAsStream("/Interface/Icons/DGAGC64.png")),
                    ImageIO.read(appClass_.getResourceAsStream("/Interface/Icons/DGAGC128.png")),
                    ImageIO.read(appClass_.getResourceAsStream("/Interface/Icons/DGAGC256.png"))
                });
        } catch (IOException ex) {
            Logger.getLogger(DgaJmeGameApplication.class.getName()).log(
                    Level.WARNING, "Failed to load application icon.", ex);
        }
        DgaJmeGameApplication app_ = new DgaJmeGameApplication();
        app_.setSettings(appSettings_);
        app_.start();
    }

    /**
     * The method performs initialization of the scene. For each object that you
     * add to the scene, remember to perform the following steps: 1) Create a
     * shape. 2) Create a geometry from the shape. 3) Create a material. 4)
     * Apply the material to the geometry. 5) Attach the geometry to the
     * rootNode.
     *
     * These are the three, main overridable methods of every game. You
     * initialize scenes with code in the simpleInitApp() method, and you define
     * interactions in the simpleUpdate() method. Advanced users can override
     * the simpleRender() method for advanced modifications of the frameBuffer
     * object and scene graph.
     */
    @Override
    public void simpleInitApp() {
        //
        AssetManager assetManager_ = this.assetManager;
        BulletAppState bulletAppState_ = new BulletAppState();
        //bulletAppState_ = new BulletAppState(PhysicsSpace.BroadphaseType.DBVT); // DBVT is dynamic scale of World Size
        //bulletAppState_.setThreadingType(BulletAppState.ThreadingType.PARALLEL);  // use physics on a separete thread
        this.stateManager.attach(bulletAppState_);
        PhysicsSpace physicsSpace_ = bulletAppState_.getPhysicsSpace();
        Node rootNode_ = (Node) viewPort.getScenes().get(0);
        //
        //garageState_ = new GarageAppState();
        //stateManager.attach(garageState_);
        //flyCam.setEnabled(false);
        //
        this.guiFont = this.assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch_ = new BitmapText(guiFont, false);
        ch_.setSize(this.guiFont.getCharSet().getRenderedSize());
        ch_.setText("Use W-A-S-D to move the battle tank, LeftClick - the cannon shot, V - the machine gun shot, Esc to exit");
        ch_.setColor(new ColorRGBA(1.0f, 0.8f, 0.1f, 1.0f));
        ch_.setLocalTranslation(settings.getWidth() * 0.3f, settings.getHeight() * 0.1f, 0f);
        this.guiNode.attachChild(ch_);
        this.viewPort.setBackgroundColor(ColorRGBA.Gray);
        //
        DgaJmeScale scaleBf_ = DefaultDgaJmeScale.KOELN_BATTLEFIELD_SCALE;
        BattlefieldModel battlefieldModel_ = new DefaultBattlefieldModel(
            assetManager_, physicsSpace_, rootNode, this.stateManager, scaleBf_);
        Battlefield battlefield_ = new Koeln1945(battlefieldModel_);
        //
        KampfpanzerModel panzer1Model_ = new DefaultKampfpanzerModel(
            assetManager_, physicsSpace_, rootNode, this.stateManager, this.renderManager, 
            this.settings, this.guiNode, DefaultDgaJmeScale.KOELN_KOMBATTANT_SCALE);
        panzer1Model_.setInitialPosition(new Vector3f(0f, 0f, 0f));
        panzer1Model_.setInitialRotation(
            new Quaternion().fromAngleAxis(FastMath.HALF_PI, Vector3f.UNIT_Y));
        panzer1Model_.setId("Panther-234");
        PzKpfwVPantherAusfD_FP panzer1_ = new PzKpfwVPantherAusfD_FP(panzer1Model_);
        panzer1_.setGranateScale(DefaultDgaJmeScale.KOELN_GRANATE_SCALE);
        panzer1_.setGeschossScale(DefaultDgaJmeScale.KOELN_GESCHOSS_SCALE);
        //
        this.stateManager.attach(battlefield_);
        this.stateManager.attach(panzer1_);
        battlefield_.addProponent(panzer1_);
        //
        DirectionalLight dl_ = new DirectionalLight();
        rootNode_.addLight(dl_);
        AmbientLight al_ = new AmbientLight();
        al_.setColor(new ColorRGBA(1.0f, 1.0f, 1.5f, 1.0f));
        rootNode_.addLight(al_);
        //
        if (IS_IN_COLLISION_DEV_MODE == true) {
            this.stateManager.getState(BulletAppState.class).setDebugEnabled(true);
        } else {
            this.stateManager.getState(BulletAppState.class).setDebugEnabled(false);
        }
    }

    /**
     * In this method interactions between the shapes are defined.
     *
     * @param tpf This float is always set to the current time per frame (tpf),
     * equal to the number of seconds it took to update and render the last
     * video frame. You can use this value in the method body to time actions,
     * such as this rotation, depending on the speed of the user's hardware—the
     * tpf is high on slow computers, and low on fast computers. This means, the
     * cube rotates in few, wide steps on slow computers, and in many, tiny
     * steps on fast computers.
     */
    @Override
    public void simpleUpdate(final float tpf) {
        super.simpleUpdate(tpf);
        //TODO: add simple update code
    }

    /**
     * The method acomplishes an advanced renderer/frameBuffer for advanced
     * modifications of the frameBuffer object and scene graph.
     *
     * @param rm RenderManager takes care of rendering the scene graphs attached
     * to each viewport and handling SceneProcessors.
     */
    @Override
    public void simpleRender(final RenderManager rm) {
        //TODO: add render code
    }
    //
    // *************************************************************************
    //
    /**
     * The method indicates whether some other object is "equal to" this one.
     *
     * @param obj The reference object with which to compare.
     *
     * @return boolean <code>true</code> if this object is the same as the obj
     * argument, or <code>false</code> otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DgaJmeGameApplication OTHER_ = (DgaJmeGameApplication) obj;
        return this.APP_ID_.equals(OTHER_.APP_ID_);
    }

    /**
     * The method returns a hash code value for the object.
     *
     * @return int A hash code value for this object.
     */
    @Override
    public int hashCode() {
        int hash_ = 7;
        hash_ = 23 * hash_ + APP_ID_.hashCode();
        return hash_;
    }

    /**
     * The method returns a string representation of the object.
     *
     * @return String A string representation of the object.
     */
    @Override
    public String toString() {
        return new StringBuilder("DgaJmeGameApplication { ")
            .append("ID = ").append(APP_ID_)
            .append(" }").toString();
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
