/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import java.util.List;

/**
 * The common interface for game objects having supplementary equipment.
 *
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.08.2018
 */
public interface DgaJmeEquipmentable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method adds attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @param attachedEquipment Supplementary equipment attached to the game object.
     */
    public void addAttachedEquipment(final DgaJmeModel... attachedEquipment);
    
//    /**
//     * The method adds attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @param attachedEquipment Supplementary equipment attached to the game 
//     * object with the distance between the game object and the equipment.
//     */
//    public void addAttachedEquipment(final Map.Entry<DgaJmeModel, Float>... attachedEquipment);
    
    /**
     * The method adds distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @param distantEquipment Supplementary equipment tied to the game object.
     */
    public void addDistantEquipment(final DgaJmeModel... distantEquipment);
    
//    /**
//     * The method adds distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @param distantEquipment Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    public void addDistantEquipment(final Map.Entry<DgaJmeModel, Float>... distantEquipment);
    
    /**
     * The method returns attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @return List<DgaJmeModel> Supplementary equipment attached to the game object.
     */
    public List<DgaJmeModel> getAttachedEquipmentList();
    
//    /**
//     * The method returns attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @return Map<M, Float> Supplementary equipment attached to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    public Map<DgaJmeModel, Float> getAttachedEquipmentMap();
    
    /**
     * The method returns distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @return List<DgaJmeModel> Supplementary equipment tied to the game object.
     */
    public List<DgaJmeModel> getDistantEquipmentList();
    
//    /**
//     * The method returns distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @return Map<M, Float> Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    public Map<DgaJmeModel, Float> getDistantEquipmentMap();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}