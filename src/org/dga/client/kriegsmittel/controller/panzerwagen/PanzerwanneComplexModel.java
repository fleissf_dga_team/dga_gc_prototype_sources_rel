/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller.panzerwagen;

import com.jme3.scene.Spatial;

/**
 * The interface of a complex model for an armored weapon's hull.
 * 
 * @extends PanzerwanneModel
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 6.7.2018
 */
public interface PanzerwanneComplexModel extends PanzerwanneModel {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the left side orientation point.
     * 
     * @return Spatial The left side orientation point.
     */
    public Spatial getSeitenpunktLinke();

    /**
     * The method sets the left side orientation point.
     * 
     * @param seitenpunktLinke The left side orientation point.
     */
    public void setSeitenpunktLinke(final Spatial seitenpunktLinke);

    /**
     * The method returns the flag whether the model has a left side orientation point or not.
     * 
     * @return boolean The flag whether the model has a left side orientation point or not.
     */
    public boolean hasSeitenpunktLinke();

    /**
     * The method returns the right side orientation point.
     * 
     * @return Spatial The right side orientation point.
     */
    public Spatial getSeitenpunktRechte();

    /**
     * The method sets the right side orientation point.
     * 
     * @param seitenpunktRechte The right side orientation point.
     */
    public void setSeitenpunktRechte(final Spatial seitenpunktRechte);

    /**
     * The method returns the flag whether the model has a right side orientation point or not.
     * 
     * @return boolean The flag whether the model has a right side orientation point or not.
     */
    public boolean hasSeitenpunktRechte();

    /**
     * The method returns the rear orientation point.
     * 
     * @return Spatial The rear orientation point.
     */
    public Spatial getHinterpunkt();

    /**
     * The method sets the rear orientation point.
     * 
     * @param hinterpunkt The rear orientation point.
     */
    public void setHinterpunkt(final Spatial hinterpunkt);

    /**
     * The method returns the flag whether the model has a rear orientation point or not.
     * 
     * @return boolean The flag whether the model has a rear orientation point or not.
     */
    public boolean hasHinterpunkt();
    
    /**
     * The method returns the left upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The left upper glacis armoring value of the armored weapon's hull.
     */
    public float getLeftUpperGlacisArmoring();

    /**
     * The method sets the left upper glacis armoring value of the armored weapon's hull.
     * 
     * @param leftUpperGlacisArmoring The left upper glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setLeftUpperGlacisArmoring(final float leftUpperGlacisArmoring);
    
    /**
     * The method returns the left lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The left lower glacis armoring value of the armored weapon's hull.
     */
    public float getLeftLowerGlacisArmoring();

    /**
     * The method sets the left lower glacis armoring value of the armored weapon's hull.
     * 
     * @param leftLowerGlacisArmoring The left lower glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setLeftGlacisArmoring(final float leftLowerGlacisArmoring);
    
    /**
     * The method returns the right upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The right upper glacis armoring value of the armored weapon's hull.
     */
    public float getRightUpperGlacisArmoring();

    /**
     * The method sets the right upper glacis armoring value of the armored weapon's hull.
     * 
     * @param rightUpperGlacisArmoring The right upper glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setRightUpperGlacisArmoring(final float rightUpperGlacisArmoring);

    /**
     * The method returns the right lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The right lower glacis armoring value of the armored weapon's hull.
     */
    public float getRightLowerGlacisArmoring();

    /**
     * The method sets the right lower glacis armoring value of the armored weapon's hull.
     * 
     * @param rightLowerGlacisArmoring The right lower glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setRightLowerGlacisArmoring(final float rightLowerGlacisArmoring);
    
    /**
     * The method returns the rear upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The rear upper glacis armoring value of the armored weapon's hull.
     */
    public float getRearUpperGlacisArmoring();

    /**
     * The method sets the rear upper glacis armoring value of the armored weapon's hull.
     * 
     * @param rearUpperGlacisArmoring The rear upper glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setRearUpperGlacisArmoring(final float rearUpperGlacisArmoring);

    /**
     * The method returns the rear lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The rear lower glacis armoring value of the armored weapon's hull.
     */
    public float getRearLowerGlacisArmoring();

    /**
     * The method sets the rear lower glacis armoring value of the armored weapon's hull.
     * 
     * @param rearLowerGlacisArmoring The rear lower glacis armoring value of 
     * the armored weapon's hull.
     */
    public void setRearLowerGlacisArmoring(final float rearLowerGlacisArmoring);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
