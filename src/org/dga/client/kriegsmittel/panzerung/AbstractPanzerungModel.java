/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.panzerung;

import org.dga.client.kombattant.Kombattant;
import org.dga.client.kriegsmittel.AbstractKriegsmittelModel;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for an armour.
 * 
 * @extends AbstractKriegsmittelModel
 * @implements PanzerungModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 6.7.2018
 */
public abstract class AbstractPanzerungModel extends AbstractKriegsmittelModel 
    implements PanzerungModel {
    private static final Vector3f CENTER_ZERO_ = Vector3f.ZERO;
    private float frontArmoring_ = 0f;
    private float leftArmoring_ = 0f;
    private float rightArmoring_ = 0f;
    private float rearArmoring_ = 0f;
    private float topArmoring_ = 0f;
    private float bottomArmoring_ = 0f;
    private Spatial vorpunktSpatial_ = null;
    private boolean hasVorpunkt_ = false;
    private BoundingBox measures_ = new BoundingBox(CENTER_ZERO_, 0f, 0f, 0f);
    private boolean hasMeasures_ = false;
    private float length_ = 0f;
    private float width_ = 0f;
    private float height_ = 0f;
    private float halfLength_ = 0f;
    private float halfWidth_ = 0f;
    private float halfHeight_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the model is created for.
     * @param modelSpatial The model's spatial.
     * @param modelScale The model's scale.
     * @param modelShape The model's collision shape.
     */
    public AbstractPanzerungModel(final Kombattant kombattant, 
        final Spatial modelSpatial, final DgaJmeScale modelScale, 
        final CollisionShape modelShape) {
        super(kombattant, modelSpatial, modelScale, modelShape);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the front armoring value.
     * 
     * @return float The front armoring value.
     */
    @Override
    public float getFrontArmoring() {
        return frontArmoring_;
    }

    /**
     * The method sets the front armoring value.
     * 
     * @param frontArmoring The front armoring value.
     */
    @Override
    public void setFrontArmoring(final float frontArmoring) {
        frontArmoring_ = frontArmoring;
    }

    /**
     * The method returns the left armoring value.
     * 
     * @return float The left armoring value.
     */
    @Override
    public float getLeftArmoring() {
        return leftArmoring_;
    }

    /**
     * The method sets the left armoring value.
     * 
     * @param leftArmoring The left armoring value.
     */
    @Override
    public void setLeftArmoring(final float leftArmoring) {
        leftArmoring_ = leftArmoring;
    }

    /**
     * The method returns the right armoring value.
     * 
     * @return float The right armoring value.
     */
    @Override
    public float getRightArmoring() {
        return rightArmoring_;
    }

    /**
     * The method sets the right armoring value.
     * 
     * @param rightArmoring The right armoring value.
     */
    @Override
    public void setRightArmoring(final float rightArmoring) {
        rightArmoring_ = rightArmoring;
    }

    /**
     * The method returns the rear armoring value.
     * 
     * @return float The rear armoring value.
     */
    @Override
    public float getRearArmoring() {
        return rearArmoring_;
    }

    /**
     * The method sets the rear armoring value.
     * 
     * @param rearArmoring The rear armoring value.
     */
    @Override
    public void setRearArmoring(final float rearArmoring) {
        rearArmoring_ = rearArmoring;
    }

    /**
     * The method returns the top armoring value.
     * 
     * @return float The top armoring value.
     */
    @Override
    public float getTopArmoring() {
        return topArmoring_;
    }

    /**
     * The method sets the top armoring value.
     * 
     * @param topArmoring The top armoring value.
     */
    @Override
    public void setTopArmoring(final float topArmoring) {
        topArmoring_ = topArmoring;
    }

    /**
     * The method returns the bottom armoring value.
     * 
     * @return float The bottom armoring value.
     */
    @Override
    public float getBottomArmoring() {
        return bottomArmoring_;
    }

    /**
     * The method sets the bottom armoring value.
     * 
     * @param bottomArmoring The front armoring value.
     */
    @Override
    public void setBottomArmoring(final float bottomArmoring) {
        bottomArmoring_ = bottomArmoring;
    }
    
    /**
     * The method returns the front orientation point.
     * 
     * @return Spatial The front orientation point.
     */
    @Override
    public Spatial getVorpunkt() {
        return vorpunktSpatial_;
    }

    /**
     * The method sets the front orientation point.
     * 
     * @param vorpunktSpatial The front orientation point.
     */
    @Override
    public void setVorpunkt(final Spatial vorpunktSpatial) {
        vorpunktSpatial_ = vorpunktSpatial;
        hasVorpunkt_ = true;
    }

    /**
     * The method returns the flag whether the model has a front orientation point or not.
     * 
     * @return boolean The flag whether the model has a front orientation point or not.
     */
    @Override
    public boolean hasVorpunkt() {
        return hasVorpunkt_;
    }
    
    /**
     * The method sets the bounding box defining the model's measures.
     * 
     * @param measures The bounding box defining the model's measures.
     */
    @Override
    public void setBoundingBox(final BoundingBox measures) {
        if (measures == null) {
            return;
        }
        measures_ = measures;
        this.calculateMeasures();
    }
    
    /**
     * The method returns the model's geometric center updating during the movement.
     * 
     * @return Vector3f The model's geometric center updating during the movement.
     */
    @Override
    public Vector3f getCenter() {
        if (hasMeasures_ == false) {
            return CENTER_ZERO_;
        }
        return measures_.getCenter();
    }
    
    /**
     * The method returns the model's length.
     * 
     * @return float The model's length.
     */
    @Override
    public float getLength() {
        return length_;
    }
    
    /**
     * The method returns the model's width.
     * 
     * @return float The model's width.
     */
    @Override
    public float getWidth() {
        return width_;
    }
    
    /**
     * The method returns the model's height.
     * 
     * @return float The model's height.
     */
    @Override
    public float getHeight() {
        return height_;
    }
    
    /**
     * The method returns the model's half-length.
     * 
     * @return float The model's half-length.
     */
    @Override
    public float getHalfLength() {
        return halfLength_;
    }
    
    /**
     * The method returns the model's half-width.
     * 
     * @return float The model's half-width.
     */
    @Override
    public float getHalfWidth() {
        return halfWidth_;
    }
    
    /**
     * The method returns the model's half-height.
     * 
     * @return float The model's half-height.
     */
    @Override
    public float getHalfHeight() {
        return halfHeight_;
    }
    
    /**
     * The method returns the flag whether the model has measures or not.
     * 
     * @return boolean The flag whether the model has measures or not.
     */
    @Override
    public boolean hasMeasures() {
        return hasMeasures_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method calculates the model's measures.
     */
    private void calculateMeasures() {
        length_ = measures_.getZExtent();
        width_ = measures_.getXExtent();
        height_ = measures_.getYExtent();
        halfLength_ = length_ / 2.0f;
        halfWidth_ = width_ / 2.0f;
        halfHeight_ = height_ / 2.0f;
        hasMeasures_ = true;
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
