/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.panzerung;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The default implementation of a physics model for an armour.
 * 
 * @extends AbstractPanzerungModel
 * @implements PanzerungPhysicsModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.07.2018
 */
public final class DefaultPanzerungPhysicsModel extends AbstractPanzerungModel 
    implements PanzerungPhysicsModel {
    private float modelMass_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the model is created for.
     * @param modelSpatial The model's spatial.
     * @param modelScale The model's scale.
     * @param modelShape The model's collision shape.
     * @param modelMass The model's mass.
     */
    public DefaultPanzerungPhysicsModel(final Kombattant kombattant, 
        final Spatial modelSpatial, final DgaJmeScale modelScale, 
        final CollisionShape modelShape, final float modelMass) {
        super(kombattant, modelSpatial, modelScale, modelShape);
        modelMass_ = modelMass;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the model's mass in kilograms.
     * 
     * @return float The model's mass in kilograms.
     */
    @Override
    public float getMass() {
        return modelMass_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}