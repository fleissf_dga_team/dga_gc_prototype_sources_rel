/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.DgaJmeCommonConstant;
import com.jme3.math.FastMath;

/**
 * The common constants for weapon.
 *
 * @extends DgaJmeCommonConstant
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public class KriegsmittelCommonConstant extends DgaJmeCommonConstant {
    /**
     * The default fire vertical angle used for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_FIRE_VERTICAL_ANGLE = 
        new KriegsmittelCommonConstant(90.0f);
    /**
     * The default fire sector angle used for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_FIRE_SECTOR_ANGLE = 
        new KriegsmittelCommonConstant(360.0f);
    /**
     * Maximum elevation angle for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_SCHUSSWAFFE_MAX_ELEVATION_ANGLE = 
        new KriegsmittelCommonConstant(45.0f);
    /**
     * Maximum depression angle for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_SCHUSSWAFFE_MAX_DEPRESSION_ANGLE = 
        new KriegsmittelCommonConstant(-45.0f);
    /**
     * Fire sector's horizontal angle for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_SCHUSSWAFFE_FIRE_SECTOR_ANGLE = 
        new KriegsmittelCommonConstant(45.0f);
    /**
     * The default load time for shooting armaments.
     */
    public static final KriegsmittelCommonConstant DEFAULT_SCHUSSWAFFE_LOAD_TIME = 
        new KriegsmittelCommonConstant(10.0f);
    //
    private float floatValue_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The float constructor.
     *
     * @param value The constant's float value.
     */
    protected KriegsmittelCommonConstant(final float value) {
        super(value);
        floatValue_ = value;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a float value.
     *
     * @return float A float value.
     */
    @Override
    public float getFloatValue() {
        return floatValue_;
    }

    /**
     * The method returns a value in radians.
     *
     * @return float A value in radians.
     */
    @Override
    public float getRadiansValue() {
        return floatValue_ * FastMath.DEG_TO_RAD;
    }
    
    /**
     * The method returns an integer value.
     *
     * @return int An integer value.
     */
    @Override
    public int getIntValue() {
        return Math.round(floatValue_);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
