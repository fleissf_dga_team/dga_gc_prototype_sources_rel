/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

/**
 * The coordinate system's axes.
 *
 * @extends java.lang.Object
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 5.6.2018
 */
public enum DgaJmeAxis {
    /**
     * The X axis.
     */
    AXIS_X(0.0f),
    /**
     * The Y axis.
     */
    AXIS_Y(1.0f),
    /**
     * The Z axis.
     */
    AXIS_Z(2.0f),
    /**
     * The W axis.
     */
    AXIS_W(3.0f);
    //
    private float floatValue_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     *
     * @param value The constant's float value.
     */
    private DgaJmeAxis(final float value) {
        floatValue_ = value;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns a float value.
     *
     * @return float A float value.
     */
    public float getFloatValue() {
        return floatValue_;
    }
    
    /**
     * The method returns an integer value.
     *
     * @return int An integer value.
     */
    public int getIntValue() {
        return Math.round(floatValue_);
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
