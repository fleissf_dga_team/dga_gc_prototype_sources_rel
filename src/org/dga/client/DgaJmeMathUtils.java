/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.math.FastMath;

/**
 * The utility class for common methods and functions.
 * 
 * @extends java.lang.Object
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 3.6.2018
 */
public final class DgaJmeMathUtils extends Object {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The default constructor.
     */
    private DgaJmeMathUtils() {
        super();
    }
    //
    // *************************** Public Methods ******************************
    //
    // ****************** Circle and circumference utilities *******************
    /**
     * The method returns the length of the circle's chord calculated by central 
     * angle and radius. A chord of a circle is a line segment that connects one 
     * point on the edge of the circle with another point on the circle. The 
     * circle's diameter is just the longest chord of this circle.
     * 
     * @param radius The circle's radius.
     * @param centralAngle The central angle.
     * @return float The length of the circle's chord calculated by central angle 
     * and radius.
     */
    public static float calculateCircleChord(final float radius, final float centralAngle) {
        float chord_ = 2.0f * radius * FastMath.sin(centralAngle / 2.0f);
        return chord_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
