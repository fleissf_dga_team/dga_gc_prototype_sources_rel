/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

/**
 * The root implementation for game characters' scale.
 * 
 * @extends java.lang.Object
 * @implements DgaJmeScale
 * @pattern Adapter
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 15.7.2018
 */
public final class DefaultDgaJmeScale extends Object implements DgaJmeScale {
    /**
     * The original scale for models used within the overall game client, it equals 1.0.
     */
    public static final DgaJmeScale ORIGINAL_MODEL_SCALE = new DefaultDgaJmeScale(1.0f);
    /**
     * The default scale for models used within the overall game client, it equals 0.02.
     */
    public static final DgaJmeScale DEFAULT_MODEL_SCALE = new DefaultDgaJmeScale(0.02f);
    /**
     * The default scale for combat participators, it equals 0.02.
     */
    public static final DgaJmeScale DEFAULT_KOMBATTANT_SCALE = new DefaultDgaJmeScale(0.02f);
    /**
     * The default scale for projectiles, it equals 0.04.
     */
    public static final DgaJmeScale DEFAULT_PROJEKTIL_SCALE = new DefaultDgaJmeScale(0.04f);
    /**
     * The default scale for shells, it equals 0.04.
     */
    public static final DgaJmeScale DEFAULT_GRANATE_SCALE = new DefaultDgaJmeScale(0.04f);
    /**
     * The default scale for bullets, it equals 0.06.
     */
    public static final DgaJmeScale DEFAULT_GESCHOSS_SCALE = new DefaultDgaJmeScale(0.06f);
    /**
     * The default scale for battlefields, it equals 1.0.
     */
    public static final DgaJmeScale DEFAULT_BATTLEFIELD_SCALE = new DefaultDgaJmeScale(1.0f);
    /**
     * The default scale for target cursors, it equals 1.0.
     */
    public static final DgaJmeScale DEFAULT_TARGET_CURSOR_SCALE = new DefaultDgaJmeScale(1.0f);
    /**
     * The scale for the battlefield Koeln, it equals 0.8.
     */
    public static final DgaJmeScale KOELN_BATTLEFIELD_SCALE = new DefaultDgaJmeScale(0.8f);
    /**
     * The scale for combat participators on the battlefield Koeln, it equals 0.02.
     */
    public static final DgaJmeScale KOELN_KOMBATTANT_SCALE = new DefaultDgaJmeScale(0.02f);
    /**
     * The scale for shells on the battlefield Koeln, it equals 0.04.
     */
    public static final DgaJmeScale KOELN_GRANATE_SCALE = new DefaultDgaJmeScale(0.04f);
    /**
     * The scale for bullets on the battlefield Koeln, it equals 0.06.
     */
    public static final DgaJmeScale KOELN_GESCHOSS_SCALE = new DefaultDgaJmeScale(0.06f);
    /**
     * The scale for a target cursor on the battlefield Koeln, it equals 1.0.
     */
    public static final DgaJmeScale KOELN_TARGET_CURSOR_SCALE = new DefaultDgaJmeScale(1.0f);
    //
    private float floatValue_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new scale with the supplied properties.
     * 
     * @param floatValue The float value.
     */
    protected DefaultDgaJmeScale(final float floatValue) {
        super();
        floatValue_ = floatValue;
    }
    
    /**
     * The constructor creates a new scale with the supplied properties.
     * 
     * @param other Other game character's scale.
     */
    protected DefaultDgaJmeScale(final DefaultDgaJmeScale other) {
        this(other.getFloatValue());
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the scale's float value.
     * 
     * @return float The scale's float value.
     */
    @Override
    public float getFloatValue() {
        return floatValue_;
    }
    //
    // *************************************************************************
    //
    /**
     * The method indicates whether some other object is "equal to" this one.
     *
     * @param obj The reference object with which to compare.
     *
     * @return boolean <code>true</code> if this object is the same as the obj 
     * argument, or <code>false</code> otherwise.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if ((obj instanceof DgaJmeScale) == false) {
            return false;
        }
        return this.floatValue_ == ((DgaJmeScale) obj).getFloatValue();
    }

    /**
     * The method returns a hash code value for the object.
     *
     * @return int A hash code value for this object.
     */
    @Override
    public int hashCode() {
        int hash_ = 7;
        hash_ = 23 * hash_ + Float.valueOf(floatValue_).hashCode();
        return hash_;
    }

    /**
     * The method returns a string representation of the object.
     *
     * @return String A string representation of the object.
     */
    @Override
    public String toString() {
        return new StringBuilder("DefaultDgaJmeScale { ")
            .append("floatValue = ").append(floatValue_)
            .append(" }").toString();
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
