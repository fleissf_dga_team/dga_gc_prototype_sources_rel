/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone.mm75;

import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kombattant.Kombattant;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone.AbstractDeuPanzerhartkerngranate;
import org.dga.client.kriegsmittel.projektil.granate.DefaultGranateModel;
import org.dga.client.kriegsmittel.projektil.granate.Panzerhartkerngranate;
import org.dga.client.kraftwerk.DefaultSchaden;
import org.dga.client.kraftwerk.Schaden;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import org.dga.client.DgaJmeScale;

/**
 * The german shell 7,5 cm Panzergranate 40 (HK) Pzgr.40/42 used for the battle 
 * tank cannon 7,5 cm Kampfwagenkanone Kw.K. 42 L/70 (75mm KwK L/70).
 *
 * @extends AbstractDeuPanzerhartkerngranate
 * @implements Panzerhartkerngranate
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 10.5.2018
 */
public final class Pzgr40_42 extends AbstractDeuPanzerhartkerngranate implements Panzerhartkerngranate {
    // The path to the shell's model.
    private static final String GRANATE_MODEL_PATH_ = 
        "Models/Kriegsteilnehmer/Kampfwagenkanone/DEU/Patrone75mm/Panzergranate40.42_Kw.K.42.mesh.j3o";
    // The shell's model name.
    private static final String GRANATE_MODEL_NAME_ = "Panzergranate 40/42";
    // The shell's mass in kilograms.
    private static final float GRANATE_MASS_ = 4.75f;
    // The shell's linear speed in meters per second.
    private static final float GRANATE_VELOCITY_ = 1120.0f;
    // The shell's damage.
    private static final Schaden GRANATE_SCHADEN_ = new DefaultSchaden(300.0f);
    //
    private AssetManager assetManager_ = null;
    private Kombattant kombattant_ = null;
    private DgaJmeScale granateScale_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new shell with the supplied properties.
     * 
     * @param assetManager The asset manager.
     * @param kombattant The combat participator the shell is created for.
     * @param granateScale The shell's scale.
     */
    public Pzgr40_42(final AssetManager assetManager, final Kombattant kombattant, 
        final DgaJmeScale granateScale) {
        super(
            new DefaultGranateModel(
                assetManager, 
                kombattant, 
                assetManager.loadModel(GRANATE_MODEL_PATH_), 
                granateScale, 
                GRANATE_MASS_));
        assetManager_ = assetManager;
        kombattant_ = kombattant;
        granateScale_ = granateScale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the name of weapon.
     * 
     * @return String The name of weapon.
     */
    @Override
    public String getName() {
        return GRANATE_MODEL_NAME_;
    }
    
    /**
     * The method returns the damage which the shell can apply to an armored 
     * weapon and by that diminish its fighting capacity.
     * 
     * @return Panzerungschaden The damage which the shell can apply to an 
     * armored weapon and by that diminish its fighting capacity.
     */
    @Override
    public Schaden getSchaden() {
        return GRANATE_SCHADEN_;
    }
    
    /**
     * The method returns the shell's velocity.
     * 
     * @return float The shell's velocity.
     */
    @Override
    public float getVelocity() {
        return GRANATE_VELOCITY_;
    }
    
    /**
     * The method creates a clone of the control, the given spatial is the
     * cloned version of the spatial to which this control is attached to.
     *
     * @param spatial The spatial.
     *
     * @return Control A clone of this control for the spatial.
     */
    @Override
    public Control cloneForSpatial(final Spatial spatial) {
        Pzgr40_42 control_ = new Pzgr40_42(assetManager_, kombattant_, granateScale_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
        return control_;
    }

    /**
     * The method performs a regular shallow clone of the object. Some fields
     * may also be cloned but generally only if they will never be shared with
     * other objects. (For example, local Vector3fs and so on.)
     * <p>
     * <p>
     * This method is separate from the regular clone() method so that objects
     * might still maintain their own regular java clone() semantics (perhaps
     * even using Cloner for those methods). However, because Java's clone() has
     * specific features in the sense of Object's clone() implementation, it's
     * usually best to have some path for subclasses to bypass the public
     * clone() method that might be cloning fields and instead get at the
     * superclass protected clone() methods. For example, through
     * super.jmeClone() or another protected clone method that some base class
     * eventually calls super.clone() in.</p>
     *
     * @return Object The clone of the object.
     */
    @Override
    public Object jmeClone() {
        Pzgr40_42 control_ = new Pzgr40_42(assetManager_, kombattant_, granateScale_);
//        control_.setAngularFactor(getAngularFactor());
//        control_.setAngularSleepingThreshold(getAngularSleepingThreshold());
//        control_.setCcdMotionThreshold(getCcdMotionThreshold());
//        control_.setCcdSweptSphereRadius(getCcdSweptSphereRadius());
//        control_.setCollideWithGroups(getCollideWithGroups());
//        control_.setCollisionGroup(getCollisionGroup());
//        control_.setDamping(getLinearDamping(), getAngularDamping());
//        control_.setFriction(getFriction());
//        control_.setGravity(getGravity());
//        control_.setKinematic(isKinematic());
//        control_.setKinematicSpatial(isKinematicSpatial());
//        control_.setLinearSleepingThreshold(getLinearSleepingThreshold());
//        control_.setPhysicsLocation(getPhysicsLocation(null));
//        control_.setPhysicsRotation(getPhysicsRotationMatrix(null));
//        control_.setRestitution(getRestitution());
//        if (mass > 0) {
//            control_.setAngularVelocity(getAngularVelocity());
//            control_.setLinearVelocity(getLinearVelocity());
//        }
//        control_.setApplyPhysicsLocal(isApplyPhysicsLocal());
//        control_.spatial_ = this.spatial_;
//        control_.setEnabled(isEnabled());
        return control_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
