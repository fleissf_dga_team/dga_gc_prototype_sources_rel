/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.patrone;

import org.dga.client.kriegsmittel.projektil.granate.AbstractPanzerhartkerngranate;
import org.dga.client.kriegsmittel.projektil.granate.GranateModel;
import org.dga.client.kriegsmittel.projektil.granate.Panzerhartkerngranate;

/**
 * The implementation of german armour-piercing capped ballistic cap (APCBC) shells.
 *
 * @extends AbstractPanzerhartkerngranate
 * @implements Panzerhartkerngranate
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 12.5.2018
 */
public abstract class AbstractDeuPanzerhartkerngranate extends AbstractPanzerhartkerngranate 
    implements Panzerhartkerngranate {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new shell with the supplied properties.
     * 
     * @param granateModel The shell's model.
     */
    public AbstractDeuPanzerhartkerngranate(final GranateModel granateModel) {
        super(granateModel);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
