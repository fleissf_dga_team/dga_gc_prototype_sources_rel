/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfpanzer.deu.PzKpfwVPanther;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.ui.Picture;
import org.dga.client.effekt.DefaultEngineEffect;
import org.dga.client.effekt.EngineEffect;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenSniperCursorModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.KampfpanzerModel;
import org.dga.client.kriegsteilnehmer.kampfpanzer.deu.AbstractDeuKampfpanzer;

/**
 * The abstract implementation for the german battle tank Panzerkampfwagen V "Panther" (Sd.Kfz. 171).
 *
 * @extends AbstractDeutscherKampfpanzer
 * @implements Kampfpanzer
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 4.5.2018
 */
public abstract class AbstractPzKpfwVPanther extends AbstractDeuKampfpanzer 
    implements PzKpfwVPanther {
    private AssetManager assetManager_ = null;
    private AppSettings appSettings_ = null;
    private Node appGuiNode_ = null;
    //
    private Picture imageSC_ = null;
    private Node nodeSC_ = null;
    private PanzerwagenSniperCursorModel modelSC_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param kampfpanzerModel The battle tank's model.
     */
    public AbstractPzKpfwVPanther(final KampfpanzerModel kampfpanzerModel) {
        super(kampfpanzerModel);
        assetManager_ = kampfpanzerModel.getAssetManager();
        appSettings_ = kampfpanzerModel.getAppSettings();
        appGuiNode_ = kampfpanzerModel.getGuiNode();
        modelSC_ = kampfpanzerModel.getSniperCursorModel();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method initializes the armoured fighting carriage's sniper cursor.
     */
    @Override
    public void initSniperCursor() {
        if (modelSC_ != null) {
            nodeSC_ = modelSC_.getNode();
        } else {
            imageSC_ = new Picture("SniperCursor");
            imageSC_.setWidth(256.0f);
            imageSC_.setHeight(256.0f);
            imageSC_.center();
            imageSC_.setImage(
                assetManager_, 
                "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPanther/Cursors/PantherSniperGreen.png", 
                true);
            nodeSC_ = new Node("SniperCursor");
            nodeSC_.attachChild(imageSC_);
        }
        nodeSC_.setLocalTranslation(
            appSettings_.getWidth() / 2.0f, appSettings_.getHeight() / 2.0f, 0f);
        appGuiNode_.attachChild(nodeSC_);
    }
    
    /**
     * The method returns an effect provided by an engine or motor that is a machine 
     * designed to convert one form of energy into mechanical energy.
     * 
     * @return EngineEffect An effect provided by an engine or motor.
     */
    @Override
    public EngineEffect getEngineEffect() {
        return DefaultEngineEffect.newBuilder(assetManager_)
            .addExhaustOne()
            .addExhaustTwo()
            .addBlastOne()
            .addBlastTwo()
            .addStandingSound(
                new AudioNode(
                    assetManager_, 
                    "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPanther/Sounds/PantherStanding.wav", 
                    AudioData.DataType.Buffer))
            .addRunningSound(
                new AudioNode(
                    assetManager_, 
                    "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPanther/Sounds/PantherRunning.wav", 
                    AudioData.DataType.Buffer))
            .build();
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
