/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfpanzer.deu.PzKpfwVPanther;

import org.dga.client.kriegsteilnehmer.kampfpanzer.deu.DeuKampfpanzer;
import org.dga.client.DgaJmeScale;

/**
 * The german battle tank Panzerkampfwagen V "Panther" (Sd.Kfz. 171).
 *
 * @extends DeutscheKampfpanzer
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 16.7.2018
 */
public interface PzKpfwVPanther extends DeuKampfpanzer {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method sets a scale for the battle tank's shells.
     * 
     * @param granateScale A scale for the battle tank's shells.
     */
    public void setGranateScale(final DgaJmeScale granateScale);
    
    /**
     * The method sets a scale for the battle tank's bullets.
     * 
     * @param geschossScale A scale for the battle tank's bullets.
     */
    public void setGeschossScale(final DgaJmeScale geschossScale);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
