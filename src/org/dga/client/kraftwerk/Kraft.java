/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kraftwerk;

/**
 * The interface of game characters' power (combat readiness, fighting capacity, 
 * health power, living power, etc.).
 * 
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 7.7.2018
 */
public interface Kraft {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the initial value of the game character's power.
     * 
     * @return float The initial value of the game character's power.
     */
    public float getInitialFloatValue();
    
    /**
     * The method returns the current value of the game character's power.
     * 
     * @return float The current value of the game character's power.
     */
    public float getCurrentFloatValue();
    
    /**
     * The method diminishes the game character's power.
     * 
     * @param value The value the game character's power is going to be diminished.
     */
    public void diminish(final float value);
    
    /**
     * The method restores the game character's power.
     * 
     * @param value The value the game character's power is going to be restored.
     */
    public void restore(final float value);
    
    /**
     * The method returns the flag whether the power is completely gone or not.
     * 
     * @return boolean The flag whether the power is completely gone or not.
     */
    public boolean isGone();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
