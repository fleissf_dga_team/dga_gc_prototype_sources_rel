/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.effekt;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import org.dga.client.DgaJmeEffect;

/**
 * The interface of effects provided by an engine or motor that is a machine 
 * designed to convert one form of energy into mechanical energy.
 *
 * @extends DgaJmeEffect
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.08.2018
 */
public interface EngineEffect extends DgaJmeEffect {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the first exhaust point.
     * 
     * @return ParticleEmitter The first exhaust point.
     */
    public ParticleEmitter getExhaustOne();
    
    /**
     * The method returns the second exhaust point.
     * 
     * @return ParticleEmitter The second exhaust point.
     */
    public ParticleEmitter getExhaustTwo();
    
    /**
     * The method returns the first blast point.
     * 
     * @return ParticleEmitter The first blast point.
     */
    public ParticleEmitter getBlastOne();
    
    /**
     * The method returns the second blast point.
     * 
     * @return ParticleEmitter The second blast point.
     */
    public ParticleEmitter getBlastTwo();
    
    /**
     * The method returns the standing sound.
     * 
     * @return AudioNode The standing sound.
     */
    public AudioNode getStandingSound();
    
    /**
     * The method returns the running sound.
     * 
     * @return AudioNode The running sound.
     */
    public AudioNode getRunningSound();
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}