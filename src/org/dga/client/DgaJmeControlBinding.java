/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import com.jme3.input.controls.Trigger;

/**
 * The interface of control bindings for game control triggers (keyboard, mouse, etc).
 *
 * @param <T> The type of a game control trigger.
 * @pattern Adapter
 * @pattern Chain of Responsibility
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public interface DgaJmeControlBinding<T extends Trigger> extends DgaJmeNameable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the binding's trigger.
     * 
     * @return T The binding's trigger.
     */
    public T getTrigger();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
